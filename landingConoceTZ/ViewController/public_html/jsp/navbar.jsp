<style>
    body{ 
        padding-top: .5em;
    }
  
    #imgheaderlogo{
        height: 25px;
        width: 29px;
        margin-top: -.6em;
    }

 
    .tpr-header-texto{  
        font-family: 'Montserrat-semibold' !important;
        font-size: 1em;
        color:#3B4559;  
    } 

    .tpr-font{ 
        font-family: 'Montserrat-semibold' !important;
        font-size: 1em;
    } 
    
    #uno{
        margin-right: -6%; 
    }

    #dos{
        margin-right: 2%; 
        margin-left: 2%;
    }

    .fontmedium{
        font-family: 'Montserrat-medium' !important;
        color:   #3B4559;
    }

    .fontbold{
        font-family: 'Montserrat-bold' !important;
        color:   #3B4559;
    }

</style> 
<!--arriba-->
<div class="d-none d-lg-block main container" style="margin-top: 5px;margin-bottom: 5px;">
    <div class="row color-header">
        <div class="col-2 .col-auto" id="uno">
            <a class="" href="https://www.totalplay.com.mx"> 
                <span class="font-weight-bold tpr-font tpr-header-texto" >TOTALPLAY</span> 
            </a>
        </div>
        <div class="col-2 .col-auto" id="dos">
            <a class="" href="https://www.negociostotalplay.com.mx/"> 
                <span class="font-weight-bold tpr-font tpr-header-texto" >TOTALPLAY ZONE</span> 
            </a>
        </div>
        <div class="col" ></div>
    </div>  
</div>
<!--abajo-->
<div class="d-none d-lg-block"> 
    <nav class="navbar navbar-expand-lg  bg-color"> 
        <div class="collapse navbar-collapse" id="navbarNav"> 
            <ul class="navbar-nav" style="margin-left:70px !important;"> 
                <div class="col-4"> 
                    <li style="margin-left: -50px;"> 
                        <a class="navbar-brand" href="#"> 
                            <img id="logoTP" src="<%=request.getContextPath()%>/assets/img/logo_tz.png" class="d-inline-block align-top" alt=""> 
                        </a> 
                    </li> 
                </div> 
                <div class="col-2" style="margin-left: -13em;"> 
                    <li class="nav-item active tpr-font"> 
                        <a class="nav-link" href="#">&iquest;Qu&eacute es <br>Totalplay Zone&#63;</a> 
                    </li> 
                </div> 
                <div class="col-2" style="margin-left: -4em;"> 
                    <li class="nav-item" style="padding-top: 10px;"> 
                        <a class="nav-link" href="#">Internet Total</a> 
                    </li> 
                </div> 
            </ul> 
        </div> 
    </nav> 
</div> 