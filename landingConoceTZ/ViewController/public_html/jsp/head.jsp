<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap/bootstrap.min.css" />
    <script src="<%=request.getContextPath()%>/assets/js/jquery-3.3.1.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/popper.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/bootstrap/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/estilos.css" />
</head>